package com.itamarb.iecdemo;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.itamarb.ocr.MeterScanActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    public static final String EXTRA_LICENSE_KEY = "EXTRA_LICENSE_KEY";
    public static final String EXTRA_CONFIG_JSON = "EXTRA_CONFIG_JSON";
    public static final int SCAN_REQUEST_CODE = 2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissions();
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void checkPermissions(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
        }
        else{
            startScanning();
        }
    }

    private void startScanning(){
        try{
            Intent intent = new Intent(this, MeterScanActivity.class);
            intent.putExtra(EXTRA_LICENSE_KEY, "eyJzY29wZSI6WyJBTEwiXSwicGxhdGZvcm0iOlsiaU9TIiwiQW5kcm9pZCIsIldpbmRvd3MiLCJKUyIsIldlYiJdLCJ2YWxpZCI6IjIwMjAtMTItMjIiLCJtYWpvclZlcnNpb24iOjMsIm1heERheXNOb3RSZXBvcnRlZCI6NSwic2hvd1dhdGVybWFyayI6dHJ1ZSwicGluZ1JlcG9ydGluZyI6dHJ1ZSwiZGVidWdSZXBvcnRpbmciOiJvcHQtb3V0IiwidG9sZXJhbmNlRGF5cyI6NSwic2hvd1BvcFVwQWZ0ZXJFeHBpcnkiOnRydWUsImlvc0lkZW50aWZpZXIiOlsiY29tLml0YW1hcmIuaWVjZGVtbyJdLCJhbmRyb2lkSWRlbnRpZmllciI6WyJjb20uaXRhbWFyYi5pZWNkZW1vIl0sIndpbmRvd3NJZGVudGlmaWVyIjpbImNvbS5pdGFtYXJiLmllY2RlbW8iXSwid2ViSWRlbnRpZmllciI6WyJjb20uaXRhbWFyYi5pZWNkZW1vIl0sImpzSWRlbnRpZmllciI6WyJjb20uaXRhbWFyYi5pZWNkZW1vIl0sImltYWdlUmVwb3J0Q2FjaGluZyI6dHJ1ZSwibGljZW5zZUtleVZlcnNpb24iOjJ9CkY3bWtpQWRXeDVlRlRLaWtsbDcrNGxsazVEbTh3RW9ENWtpMlNLM0orYU94QmFtV3Q2Vm9vU3ZjZ3ovV0ZwSWZVYUZZdFluU0VWK2pwcDNaMVFtUlp5Z0JxWFhDSklsNU5qaDF0OHRoMFdrZFZjNUZTaGxtZ1dQZTJOUUNHQUJwcm1LazYrblNXQXU0K0dONE5NRFdXYmFxUTVKRlhPaGRGSThoZGRCeklXS0p0Z3p6TThvOEdCczg5MmE4aGdJS2FWYjZEYjk2U1RPb1BDb0gzRW1nVk15MjkzdEhrNFlzQ0J4MUJDS1pTRm50bnJ3ZlAwTkxNZk4vOWVGQzBXVWY1QXQ3SG4xMitXTklZVFozWlhQT1N3SDFDMHQ4WWw3Wm13dmk5WU4yVXMrN2F1djErYjFrSG8vdW44QWNERlM5Ry91TG5sQm5PcG42cVJxdXVjR1hPdz09");
            intent.putExtra(EXTRA_CONFIG_JSON, "{\"camera\":{\"captureResolution\":\"720p\"},\"flash\":{\"mode\":\"manual\",\"alignment\":\"bottom_right\"},\"cutout\":{\"style\":\"rect\",\"alignment\":\"top_half\",\"strokeWidth\":2,\"cornerRadius\":4,\"strokeColor\":\"FFFFFF\",\"outerColor\":\"000000\",\"outerAlpha\":0.3,\"feedbackStrokeColor\":\"0099FF\"},\"viewPlugin\":{\"plugin\":{\"id\":\"Meter_ID\",\"meterPlugin\":{\"scanMode\":\"AUTO_ANALOG_DIGITAL_METER\"}},\"cutoutConfig\":{\"style\":\"rect\",\"alignment\":\"top_half\",\"strokeWidth\":2,\"cornerRadius\":4,\"strokeColor\":\"FFFFFF\",\"outerColor\":\"000000\",\"outerAlpha\":0.3,\"feedbackStrokeColor\":\"0099FF\"},\"scanFeedback\":{\"style\":\"CONTOUR_RECT\",\"strokeColor\":\"0099FF\",\"fillColor\":\"220099FF\",\"blinkOnResult\":true,\"beepOnResult\":true,\"vibrateOnResult\":true},\"cancelOnResult\":true}}");
            startActivityForResult(intent, SCAN_REQUEST_CODE);
        }
        catch (Exception e){
            e.getStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startScanning();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SCAN_REQUEST_CODE) {
            String resultStr = data.getStringExtra("result");
            if(!resultStr.isEmpty()){
                if(resultCode == Activity.RESULT_OK) {
                    System.out.println("Scan was successful: " + resultStr);
                    try{
                        JSONObject result = new JSONObject(resultStr);
                        TextView resultView = findViewById(R.id.result);
                        resultView.setText(result.getString("reading"));
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    System.out.println("Scan failed. Error: " + resultStr);
                }
            }
        }
    }
}
